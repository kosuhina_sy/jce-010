package kosuhina.tm;

//import jdk.internal.jline.internal.ShutdownHooks;
import kosuhina.tm.controller.ProjectController;
import kosuhina.tm.controller.SystemController;
import kosuhina.tm.controller.TaskController;
import kosuhina.tm.entity.Project;
import kosuhina.tm.entity.Task;
import kosuhina.tm.repository.ProjectRepository;
import kosuhina.tm.repository.TaskRepository;
import kosuhina.tm.service.ProjectService;
import kosuhina.tm.service.ProjectTaskService;
import kosuhina.tm.service.TaskService;

import java.util.Scanner;

import static kosuhina.tm.constant.TerminalConst.*;

/**
 * Основной класс
 */
public class Application {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService);

    private final SystemController systemController = new SystemController();


    {
        projectRepository.create("DEMO PROJECT 1");
        projectRepository.create("DEMO PROJECT 2");
        taskRepository.create("TEST TASK 1");
        taskRepository.create("TEST TASK 2");
    }

    /**
     * Точка входа
     * @param args дополнительные аргументы запуска приложения
     */

    public static void main(final String[] args) {
        final Application application = new Application();
        final Scanner scanner = new Scanner(System.in);
        application.run(args);
        application.displayWelcome();
       String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            application.run(command);
        }

    }

    public void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * Обработка консольного ввода
     * @param param
     * @return код выполнения
     */

    private int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION: return systemController.displayVersion();
            case ABOUT: return systemController.displayAbout();
            case HELP: return displayHelp();
            case EXIT: return systemController.displayExit();

            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_VIEW: return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();

            case TASK_LIST: return taskController.listTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_CREATE: return taskController.createTask();
            case TASK_VIEW: return taskController.viewTaskByIndex();
            case TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID: return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case TASK_ADD_TO_PROJECT_BY_IDS: return taskController.addTaskToProjectByIds();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS: return taskController.removeTaskToProjectByIds();
            case TASK_LIST_BY_PROJECT_IDS: return taskController.listTaskByProjectId();


            default: return systemController.displayError();
        }
    }


    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }


    public int displayHelp() {
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println();
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-clear - Remove all projects.");
        System.out.println("project-view -  View project by index.");
        System.out.println("project-remove-by-name - Remove project by name.");
        System.out.println("project-remove-by-id - Remove project by id.");
        System.out.println("project-remove-by-index - Remove project by index.");
        System.out.println("project-update-by-index - Update project by index.");
        System.out.println();
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-clear - Remove all tasks.");
        System.out.println("task-view -  View task by index.");
        System.out.println("task-remove-by-name - Remove task by name.");
        System.out.println("task-remove-by-id - Remove task by id.");
        System.out.println("task-remove-by-index - Remove task by index.");
        System.out.println("task-update-by-index - Update task by index.");
        System.out.println("task-list-by-project-id - Display task list by project id.");
        System.out.println("task-add-to-project-by-ids - Add task to project by ids.");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids.");
        System.out.println();
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }


    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }
}
