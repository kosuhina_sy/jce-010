package kosuhina.tm.service;

import kosuhina.tm.entity.Task;
import kosuhina.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(String name) {
        return taskRepository.create(name);
    }

    public Task create(String name, String description) {
        return taskRepository.create(name, description);
    }

    public Task update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if(description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

    public void clear() { taskRepository.clear(); }

    public Task findByIndex(int index) {
        return taskRepository.findByIndex(index);
    }

    public Task findByName(String name) {
        return taskRepository.findByName(name);
    }

    public Task removeByIndex(int index) {
        return taskRepository.removeByIndex(index);
    }

    public Task removeById(Long id) {
        return taskRepository.removeById(id);
    }

    public Task removeByName(String name) {
        return taskRepository.removeByName(name);
    }

    public Task findById(Long id) {
        return taskRepository.findById(id);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findByProjectIdAndId(Long projectId, Long id) {
        if(projectId == null || id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

    public List<Task> findAllByProjectId(Long projectId) {
        if(projectId == null) return null;
        return taskRepository.findAddByProjectId(projectId);
    }
}
